from app import app
from functools import wraps
from flask import jsonify, request, session, redirect, url_for, flash

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('you need to login first')
            return redirect(url_for('login'))
    return wrap

@app.route("/")
def index():
    data = {
        "a": 1,
        "b": 2
    }
    return jsonify(data)

@app.route("/login", methods=["POST"])
def login():
    information = request.get_json()
    username = information["username"]
    password = information["password"]
    if username == 'admin' and password == 'admin':
        session['logged_in'] = True
        answer = {
            "message": "you logged in",
            "status": 200
        }
        return jsonify(answer), 200
    else:
        answer = {
            "message": "your information was wrong",
            "status": 403
        }
        return jsonify(answer), 403

@app.route('/logout')
@login_required
def logout():
    session.pop('logged_in', None)
    redirect(url_for('index'))