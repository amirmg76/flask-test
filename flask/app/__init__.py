from flask import Flask

app = Flask(__name__)
app.secret_key = "it's too hard"

from app import views